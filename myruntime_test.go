package myruntime

import (
	"testing"
)

//TestIsTerminal running the test always returns IsTerminal as false. not sure why
func TestIsTerminal(t *testing.T) {
	trmnl := Trmnl{}
	isTrmnl := trmnl.IsTerminal()
	if isTrmnl {
		t.Errorf("have %v, want %v", isTrmnl, true)
	}

}

func TestIsCygwinTerminal(t *testing.T) {
	trmnl := Trmnl{}
	isCTrmnl := trmnl.IsCygwinTerminal()
	if isCTrmnl {
		t.Errorf("have %v, want %v", isCTrmnl, true)
	}

}

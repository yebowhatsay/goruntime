package myruntime

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	isatty "github.com/mattn/go-isatty"
)

//IsTerminal whether app is running on terminal or not
var IsTerminal bool

//Logger interface
type Logger interface {
	Info(args ...interface{})
	Infof(s string, args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
	Debug(args ...interface{})
}

//Trmnl defines struct with log field
type Trmnl struct {
	log Logger
}

//New constructs and returns instance of trmnl
func New(lg Logger) *Trmnl {
	t := Trmnl{}
	t.log = lg
	return &t
}

//IsTerminal returns true if app is running on terminal
func (t *Trmnl) IsTerminal() bool {
	if isatty.IsTerminal(os.Stdout.Fd()) {
		t.log.Debug("Running on Terminal")
		IsTerminal = true
		return IsTerminal
	}
	return false
}

//IsCygwinTerminal returns true if app is running on Cygwin/MSYS2 Terminal
func (t *Trmnl) IsCygwinTerminal() bool {
	if isatty.IsCygwinTerminal(os.Stdout.Fd()) {
		t.log.Debug("Running on Cygwin/MSYS2 Terminal")
		return true
	}
	return false
}

//GetUserInput returns a string entered by user
func (t *Trmnl) GetUserInput() string {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("-> ")
	input, err := reader.ReadString('\n')
	//convert CRLF to LF Carriage Return \r Line Feed \n
	input = strings.Replace(input, "\n", "", -1)

	if err != nil {
		t.log.Fatal(err)
	}
	return input
}
